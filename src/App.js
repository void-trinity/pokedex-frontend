import React from 'react';
import { HashRouter } from "react-router-dom";
import Navbar from './components/Navbar';
import Maincontent from './components/MainContent';
const App = () => {
    return (
        <HashRouter>
            <Navbar />
            <div style={{ marginTop: 80 }} className='container'>
                <Maincontent />
            </div>
        </HashRouter>
    );
}

export default App;