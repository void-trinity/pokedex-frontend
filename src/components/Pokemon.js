import React, { Component } from 'react';
import axios from 'axios';
import M from 'materialize-css';
import Selector from './Selector';
import getTypeColor from '../helpers/GetTypeColor';
import Loader from './Loader';
import modifyID from '../helpers/ModifyID';
import config from '../helpers/Config';

const BASE_URL = config();

export default class Pokemon extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            selectedForm: 0
        }
    }

    componentDidMount() {
        this.fetchPokemon();
    }

    componentDidUpdate() {
        M.AutoInit();
        if(this.props.match.params.id !== this.state.id) {
            this.fetchPokemon();
        }
    }

    fetchPokemon = async () => {
        const { id } = this.props.match.params;
        this.setState({ id });
        await axios.get(`${BASE_URL}/api/pokemon/${id}`)
            .then(({ data }) => {
                this.setState({ data: data.data, loaded: true })
            })
            .catch(error => {
                console.log(error);
                this.setState({ loaded: true });
            })
    }

    renderSelector = () => {
        const { forms } = this.state.data[0];
        if (forms.length !== 0) {
            return (
                <Selector forms={this.state.data[0].forms} changeForm={this.changeForm}/>
            )
        }
    }

    changeForm = (val) => {
        this.setState({ selectedForm: val });
    }

    renderText = () => {
        const { xText, yText } = this.state.data[0];
        const { selectedForm, version } = this.state;
        var text = version === 'X' ? xText: yText;
        return (
            <div className="col s12 indigo lighten-5" style={{ paddingRight: 20 }}>
                <p style={{ color: '#595959', fontFamily: 'roboto', fontWeight: 500, fontSize: 16 }} align='justify'>{`${text[selectedForm]}`}</p>
            </div>
        )
    }


    renderPhysicalProps = () => {
        const { physical } = this.state.data[0];
        const { selectedForm } = this.state;
        return (
            <div className="col s12 grey lighten-4" style={{ marginTop: 5 }}>
                <div className="col s6" style={{ padding: 2 }}>
                    <h6 style={styles.physicaPropsHeading}>Height</h6>
                    <h6 style={styles.physicaPropsText}>{`${physical[selectedForm].height}`}</h6>
                    <h6 style={styles.physicaPropsHeading}>Weight</h6>
                    <h6 style={styles.physicaPropsText}>{`${physical[selectedForm].weight}`}</h6>
                    <h6 style={styles.physicaPropsHeading}>Gender</h6>
                    <h6 style={styles.physicaPropsText}>{`${physical[selectedForm].gender === '' ? 'Multigender': 'Unknown'}`}</h6>
                </div>
                <div className="col s6" style={{ padding: 2 }}>
                    <h6 style={styles.physicaPropsHeading}>Category</h6>
                    <h6 style={styles.physicaPropsText}>{`${physical[selectedForm].category}`}</h6>
                    <h6 style={styles.physicaPropsHeading}>Abilities</h6>
                    <h6 style={styles.physicaPropsText}>{`${physical[selectedForm].abilities}`}</h6>
                </div>
            </div>
        )
    }


    renderSwitch = () => {
        return (
            <div className="row" style={{ marginTop: 7 }}>
                <div className="col s12" style={{ marginBottom: 10 }}>
                    <ul className="tabs indigo lighten-5">
                        <li className="tab col s6"><a href="#" className='active' onClick={() => this.setState({ version: 'X' })} style={{ color: 'black' }}>Version X</a></li>
                        <li className="tab col s6"><a href="#" onClick={() => this.setState({ version: 'Y' })} style={{ color: 'black' }}>Version Y</a></li>
                    </ul>
                    {this.renderText()}
                    {this.renderPhysicalProps()}
                </div>
            </div>
        )
    }


    renderStats = () => {
        return (
            <div className='col s12 m6 l6'>
                <h1>Stats</h1> 
            </div>
        );
    }

    renderTypesWeakness = () => {
        const { type, weakness } = this.state.data[0];
        const { selectedForm } = this.state;
        return(
            <div className='col s12 m6 l6 orange lighten-4' style={{ paddingLeft: 20, borderRadius: 5 }}>
                <div className='row'>
                    <div>
                        <h5>Types</h5>
                        <div className='col s12' style={{ paddingLeft: 0 }}>
                            {type[selectedForm].map((item, index) => {
                                return (
                                    <span key={index} style={{ backgroundColor: getTypeColor(item), borderRadius: 2, width: '20%', float: 'left', color: 'white', marginRight: 10, marginLeft: 0 }} className="badge">{`${item}`}</span>
                                )
                            })}
                        </div>
                    </div>
                    <div style={{ marginTop: 50 }}>
                        <h5>Weaknesses</h5>
                        <div className='col s12' style={{ paddingLeft: 0 }}>
                            {weakness[selectedForm].map((item, index) => {
                                return (
                                    <span key={index} style={{ backgroundColor: getTypeColor(item), borderRadius: 2, width: '20%', float: 'left', color: 'white', marginRight: 10, marginLeft: 0 }} className="badge" className="badge">{`${item}`}</span>  
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        );
    }


    renderPokemonDetails = () => {
        const { selectedForm } = this.state;
        const { name, id, image } = this.state.data[0];
        var imgURL = '';
        if(parseInt(selectedForm) === 0) {
            imgURL = `${BASE_URL}${image}`;
        } else {
            imgURL = `${BASE_URL}/assets/images/${id}_f${parseInt(selectedForm)+1}.png`;
        }
        return (
                <div className='row'>
                    <div className='col s12'>
                        <h4 style={{ fontWeight: 'bold', color: '#6d6d6d' }}>{`${name} ${modifyID(id)}`}</h4>
                    </div>
                    <div className='col s12'>
                        <div className='card col s12 m5 l5' style={{ backgroundColor: '#ededed' }}>
                            <img src={imgURL} style={{ maxWidth: '100%' }}/>
                        </div>
                        <div className='col s1'/>
                        <div className='col s12 m6 l6' >
                            <div className='row'>
                                {this.renderSelector()}
                                {this.renderSwitch()}
                            </div>
                        </div>
                    </div>
                    <div className='col s12'>
                        {this.renderStats()}
                        {this.renderTypesWeakness()}
                    </div>
                </div>
        )
    }

    render() {
        if(this.state.loaded) {
            if(this.state.data.length) {
                return (
                    this.renderPokemonDetails()
                )
            } else {
                return (
                    <h1>404 Not Found</h1>
                )
            }
        } else {
                return (
                    <Loader />
                )
        }
    }
}

const styles = {
    physicaPropsHeading: {
        fontWeight: 500,
        fontFamily: 'roboto',
        color: '#232323'
    },
    physicaPropsText: {
        fontFamily: 'roboto',
        fontWeight: 400,
        color: '#303030'
    }
}