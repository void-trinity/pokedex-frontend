import React from 'react';
import axios from 'axios';
import PokemonCard from './PokemonCard';
import Loader from './Loader';
import config from '../helpers/Config';

const BASE_URL = config();


export default class Pokemons extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            offset: 1
        };
    }

    componentDidMount() {
        this.loadContent();
    }

    loadContent = async () => {
        let { offset } = this.state;
        await axios.post(`${BASE_URL}/api/pokemons/`, { offset })
            .then(({ data }) => {
                this.setState({ data: data.data, loaded: true });
            })
            .catch(error => {
                console.log(error);
                this.setState({ loaded: true });
            });
    }

    openPokemonDetails = (id) => {
        const { history } = this.props;
        history.push(`/pokedex/${id}`);
    }


    listItems = () => {
        const { data } = this.state;
        return data.docs.map((item, index) => {
            return (
                <PokemonCard openPokemonDetails={this.openPokemonDetails} key={index} data={item}/>
            )
        })
    }


    changeSelected = async (offset) => {
        if(offset !== this.state.offset && offset >= 1 && offset <= 68) {
            await this.setState({ offset, loaded: false });
            this.loadContent();
        }
    }

    renderPagination = () => {
        const { offset } = this.state;
        let arr = [], max, min, left, right;
        if(offset <= 3) {
            min = 1;
            max = 5;
            left = offset === 1? false: true;
            right = true;
        } else if (offset >= 64) {
            min = 64;
            max = 68;
            left = true;
            right = offset === 68? false: true;
        } else {
            min = offset-2;
            max = offset+2;
            left = true;
            right = true;
        }
        for(var i = min; i <= max; i++) {
            arr.push(i);
        }
        return (
            <div style={{ textAlign: 'center' }}>
                <ul className='pagination'>
                    <li className={left ? 'waves-effect': 'disabled'}><a onClick={() => this.changeSelected(offset-1)}><i className="material-icons">chevron_left</i></a></li>
                    {arr.map((item, index) => {
                        return (
                            <li key={index} className={offset===item?'active black': 'waves-effect'} style={{ margin: 2 }}><a onClick={() => this.changeSelected(item)}>{item}</a></li>
                        )
                    })}
                    <li className={right ? 'waves-effect': 'disabled'}><a onClick={() => this.changeSelected(offset+1)}><i className="material-icons">chevron_right</i></a></li>
                </ul>
            </div>
        )
    }

    renderHeader = () => {
        return (
            <div className='col s12' style={{ padding: 0 }}>
                <div className='col s12 m4 l4'>
                    <h4>Pokedéx</h4>
                </div>
                <div className='col s0 m4 l4'>
                </div>
                <div className='col s12 m4 l4'>
                    <form>
                        <div className="input-field">
                            <input id="search" type="search" placeholder='Search' value={this.state.value} onChange={e=>console.log(e.target.value)}></input>
                            <i style={{ marginTop: 3 }} className="material-icons">search</i>
                        </div>
                    </form>
                </div>
            </div>
        )
    }


    renderData = () => {
        const { data } = this.state;
        if(data && data.docs) {
            return (
                <div className='row'>
                    {this.renderHeader()}
                    {this.listItems()}
                    {this.renderPagination()}
                </div>
            )
        } else {
            return <h1>Problem with net</h1>
        }
    }

    render() {
        if (!this.state.loaded)
            return (
                <Loader />
            );
        else {
            return this.renderData();
        }
    }
}