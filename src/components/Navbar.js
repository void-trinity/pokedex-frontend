import React, { Component } from 'react';
import M from 'materialize-css';
import { NavLink } from 'react-router-dom';
import logo from '../assets/logo.png';

class Navbar extends Component {
    componentDidMount() {
        document.addEventListener('DOMContentLoaded', function() {
            M.AutoInit();
        });
    }
    render() {
        return(
            <div style={{ margin: 0 }}>
            <nav style={{ position: 'fixed', zIndex: 1, top: 0, backgroundColor: '#2E2736' }}>
                <div className="nav-wrapper" style={{ alignItems: 'center'}}>
                    <NavLink to="/" className="brand-logo"><img src={logo} alt='Pokdex' style={{ height: '30px', marginLeft: 10, marginRight: 10 }}/>Pokémon</NavLink>
                    <a href="#" data-target="mobile-demo" className="sidenav-trigger"><i className="material-icons">menu</i></a>
                    <ul  className="right hide-on-med-and-down">
                        <li><NavLink className='waves-effect' to="/pokedex">Pokédex</NavLink></li>
                        <li><NavLink className='waves-effect' to="/moves">Moves</NavLink></li>
                        <li><NavLink className='waves-effect' to="/items">Items</NavLink></li>
                    </ul>
                </div>
            </nav>
            <ul className="sidenav" id="mobile-demo">
                <li><NavLink className='waves-effect sidenav-close' to="/">Home</NavLink></li>
                <li><NavLink className='waves-effect sidenav-close' to="/pokedex">Pokédex</NavLink></li>
                <li><NavLink className='waves-effect sidenav-close' to="/moves">Moves</NavLink></li>
                <li><NavLink className='waves-effect sidenav-close' to="/items">Items</NavLink></li>
            </ul>
        </div>
        )
    }
}

export default Navbar;