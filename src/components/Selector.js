import React, { Component } from 'react';
import M from 'materialize-css';

export default class Selector extends Component {
    componentDidMount() {
        M.AutoInit();
    }
    render() {
        const { forms, changeForm } = this.props;
        return (
            <div className="input-field col s6">
                <select ref="dropdown" defaultValue='0' onChange={e => changeForm(e.target.value)}>
                    {forms.map((item, index) => {
                        return (
                            <option key={index} value={index}>{item}</option>
                        )
                    })}
                </select>
            <label>Pokemon Form</label>
        </div>
        )
    }
}