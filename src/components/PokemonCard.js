import React from 'react';
import getTypeColor from '../helpers/GetTypeColor';
import modifyID from '../helpers/ModifyID';
import config from '../helpers/Config';

const BASE_URL = config();

const PokemonCard = (props) => {
    const { data } = props;
    return (
        <div className='col s12 m6 l4' onClick={() => props.openPokemonDetails(data.id)}>
            <div className="card">
                <div className="card-image" style={{ padding: 30, backgroundColor: '#e8e8e8' }}>
                    <img src={`${BASE_URL}${data.image}`} />
                </div>
                <div className="card-content">
                    <hr style={{ width: '30%', border: '2px solid #ff3f05' }} align='left' />
                    <span className="card-title">{`${data.name}`}</span>
                    <p style={{ fontWeight: 'bold', color: '#565656' }}>{modifyID(data.id)}</p>
                    <div style={{ paddingBottom: 20, paddingTop: 10 }}>
                        {renderType(data.type[0])}
                    </div>
                </div>
            </div>
        </div>
    )
}

const renderType = (types) => {
    return types.map((item, index) => {
        return (
            <span key={index} style={{ backgroundColor: getTypeColor(item), borderRadius: 2, width: '30%', float: 'left', color: 'white', marginLeft: 0, marginRight: 10 }} className="badge">{`${item}`}</span>
        )
    })
}


export default PokemonCard;