import React from 'react';
import { Route } from 'react-router-dom';
import Home from './Home';
import Pokemons from './Pokemons';
import Move from './Moves';
import Item from './Items';
import Pokemon from './Pokemon';

const Maincontent = () => {
    return(
        <div className="content">
            <Route exact path="/" component={Home}/>
            <Route exact path="/pokedex" component={Pokemons}/>
            <Route path="/moves" component={Move}/>
            <Route path="/items" component={Item}/>
            <Route path="/pokedex/:id" component={Pokemon}/>
        </div>
    );      
}
export default Maincontent;