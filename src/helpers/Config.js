const config = () => {
    if (process.env.NODE_ENV === 'development') {
        return process.env.REACT_APP_LOCAL_BASE_URL;
    } else if (process.env.NODE_ENV === 'production') {
        return process.env.REACT_APP_HEROKU_BASE_URL
    }
}

module.exports = config;